# Welcome to Origami

Origami is a command-line BASH script for installing and managing
Folding @ Home clients.

This project is currently undergoing some updated since Origami has not been updated at its [Github source](https://github.com/zelut/origami) since 2012.

The current work of this repository is going to try and update Origami. See the [TO DO](#to do) section to see what we are planning to do.

## Installation

If you want to install the latest package version, you'll need to download origami to your computer. Just open a terminal, and run:

NOTE: These are the original instructions, however they might not perform as expected from this repository.

```
sudo apt-get install origami
```
Then to setup origami with your username and Team number:

```
sudo origami install -t TEAMNUMBER -u USERNAME
```

That's it! You're good to go!

## Usage

```python
import foobar

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

PLease read [CONTRIBUTING](./CONTRIBUTING.md) for more details

## License
[Copyright Christer Edwards](./LICENSE) 2007

# Roadmap

## Goals
* Update URLS
* Add MAKEFILE
* Operate Origami in a chroot (aka jail)
* leverage management of remote nodes (using SSH)
* Auto generate some documents
* python tcl/tk Python interface (Tkinter)
* make cross platform
	* run within Linux Subsystem for Windows
	* other linux flavors
	* run from a raspberry pi?
* create a PPA or update existing PPA.
* manage upstream merge requests.
* leverage gitlab CI/CD
	* run some basic install processes and tests
* explore the creation and management of docker containers for housing Origami Clients
* add new features
	*

## Design
Initial design should be simmilar to what Christer originally had.

## To Do
This section attempts to loosely define a general roadmap for Origami




## Completed
* [Fork project on github](https://github.com/samperd/origami)
* Import project to gitlab (to promote Gitlabs contribution to the open source community




# Other Resources for Folding at Home



# Previous Version Details [0.7.4]

## Other Resources for Origami
* [Ubuntu Documentation](https://help.ubuntu.com/community/FoldingAtHome/origami)
* [manpages.ubuntu.com](http://manpages.ubuntu.com/manpages/bionic/man1/origami.1.html)
* [packages.ubuntu.com](https://packages.ubuntu.com/bionic/origami)
* [launchpad.net](https://launchpad.net/origami/trunk/0.7.4)
* About the Creator: [Christer Edwards](https://launchpad.net/~christer.edwards)
* Origami on [Github.com](https://github.com/zelut/origami)
* Origami on [zelut.org](https://zelut.org
	* [Zelut/projects/origami](https://zelut.org/projects/origami/)
	* [Related blog posts](https://blog.zelut.org/?s=origami)


## Original README
/*
**  Folding @ Home Management tool
**  Version 0.7.5
**  Edited by Johnathan Falk
**  Originally Created by Christer Edwards
**  Updated on 1-6-2011
*/

Folding @ Home management tool for Linux

  * Compatible with most major distributions
  * Set-and-forget installation
  * Easy access to status and monitoring, including stats
