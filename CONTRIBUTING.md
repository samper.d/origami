# Contributing to Origami

1. Follow GitFlow best practices:
https://nvie.com/posts/a-successful-git-branching-model/

2. Before requesting a new feature check the existing issues to see if it has
already been suggested.

3. If the issue does not exist then create one.

4. If you want to contribute a new feature then:
* create a new issue
* fork the project
* create a new branch
* test the branch
* submit a merge request